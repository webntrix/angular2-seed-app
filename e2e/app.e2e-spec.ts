import { SeedappPage } from './app.po';

describe('seedapp App', () => {
  let page: SeedappPage;

  beforeEach(() => {
    page = new SeedappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
